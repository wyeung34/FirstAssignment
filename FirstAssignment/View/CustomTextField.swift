//
//  CustomTextField.swift
//  FirstAssignment
//
//  Created by William Yeung on 8/10/21.
//

import UIKit

class CustomTextField: UITextField {
    init(ph: String) {
        super.init(frame: .zero)
        self.attributedPlaceholder = .init(string: ph, attributes: [.foregroundColor: UIColor.darkGray])
        self.backgroundColor = .secondarySystemFill
        self.textColor = .darkGray
    }
    
    required init?(coder: NSCoder) {
        fatalError()
    }
}
