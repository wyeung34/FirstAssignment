//
//  CustomButton.swift
//  FirstAssignment
//
//  Created by William Yeung on 8/10/21.
//

import UIKit

class CustomButton: UIButton {
    override init(frame: CGRect) {
        super.init(frame: frame)
    }
    
    init(title: String, bgColor: UIColor) {
        super.init(frame: .zero)
        self.init(type: .system)
        self.setTitleColor(.white, for: .normal)
        self.setTitle(title, for: .normal)
        self.backgroundColor = bgColor
    }
    
    required init?(coder: NSCoder) {
        fatalError()
    }
}
