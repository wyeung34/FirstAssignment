//
//  User.swift
//  FirstAssignment
//
//  Created by William Yeung on 8/10/21.
//

import Foundation

struct User {
    let name: String
    let age: Int?
    let gender: ViewController.Gender
}
