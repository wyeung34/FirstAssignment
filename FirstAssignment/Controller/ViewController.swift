//
//  ViewController.swift
//  FirstAssignment
//
//  Created by William Yeung on 8/10/21.
//

import UIKit

class ViewController: UIViewController {
    // MARK: - Properties
    var users = [User]()
    
    enum Gender: String {
        case male
        case female
    }
    
    // MARK: - Views
    private let nameTextField = CustomTextField(ph: "Name")
    private let ageTextField = CustomTextField(ph: "Age")
    private let genderTextField = CustomTextField(ph: "Gender")
    
    private let addButton = CustomButton(title: "Add", bgColor: .systemBlue)
    private let printButton = CustomButton(title: "Print", bgColor: .systemGreen)
    
    private lazy var formStackView: UIStackView = {
        let stack = UIStackView(arrangedSubviews: [nameTextField, ageTextField, genderTextField, addButton, printButton])
        stack.axis = .vertical
        stack.distribution = .fillEqually
        stack.spacing = 10
        stack.translatesAutoresizingMaskIntoConstraints = false
        return stack
    }()
    
    // MARK: - Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        configureUI()
        layoutUI()
        setupButtonTargets()
    }

    // MARK: - Helpers
    private func configureUI() {
        view.backgroundColor = .white
        nameTextField.becomeFirstResponder()
    }
    
    private func layoutUI() {
        view.addSubview(formStackView)
        
        NSLayoutConstraint.activate([
            formStackView.centerXAnchor.constraint(equalTo: view.centerXAnchor),
            formStackView.topAnchor.constraint(equalTo: view.safeAreaLayoutGuide.topAnchor, constant: 25),
            formStackView.widthAnchor.constraint(equalTo: view.widthAnchor, multiplier: 0.85),
            formStackView.heightAnchor.constraint(equalTo: view.heightAnchor, multiplier: 0.35)
        ])
    }
    
    private func setupButtonTargets() {
        addButton.addTarget(self, action: #selector(handleAddTapped), for: .touchUpInside)
        printButton.addTarget(self, action: #selector(handlePrintTapped), for: .touchUpInside)
    }
    
    /// Sorts Users Array Alphabetically On Name
    /// - Returns: Returns a users array sorted alphabetically based on their name
    private func sortAlphabetically() -> [User] {
        return users.sorted(by: { $0.name < $1.name })
    }
    
    /// Sorts Users Array From Youngest To Oldest
    /// - Returns: An array of users with an age in the following order; users without an age (nil) are moved to the end of the array
    private func sortYoungestToOldest() -> [User] {
        var sortedUsers = users.sorted { user1, user2 in
            return (user1.age ?? 0) < (user2.age ?? 0)
        }
        
        var nilAgeCounter = 0

        for i in 0..<sortedUsers.count where sortedUsers[i].age != nil {
            (sortedUsers[i], sortedUsers[nilAgeCounter]) = (sortedUsers[nilAgeCounter], sortedUsers[i])
            nilAgeCounter += 1
        }

        return sortedUsers
    }
    
    /// Groups users into a specific age groups
    /// - Returns: Returns a dictionary of Age Groups with a array of users who falls into that age group as the value
    private func userDictionaryOfAgeGroups() -> [String: [User]] {
        var userGroups = [String: [User]]()
        
        for user in users {
            guard let age = user.age else { userGroups["Unknown", default: []].append(user); continue }
            
            switch age {
                case 1...17: userGroups["Kids", default: []].append(user)
                case 18...35: userGroups["Young Adults", default: []].append(user)
                case 36...55: userGroups["Adults", default: []].append(user)
                default: userGroups["Senior", default: []].append(user)
            }
        }
        
        return userGroups
    }
    
    /// Gets a pair of users from two specified index positions
    /// - Parameters:
    ///   - index1: Index position of user 1
    ///   - index2: Index position of user 2
    /// - Returns: Returns a tuple containing two users from the specified indexes; if any index positions are out of bounds, a nil will be returned in its place
    private func pairFromAlphabeticalList(_ index1: Int, _ index2: Int) -> (User?, User?) {
        let sortedUsers = sortAlphabetically()
        let indexRange = 0..<sortedUsers.count

        switch (indexRange ~= index1, indexRange ~= index2) {
            case (true, true):     return (sortedUsers[index1], sortedUsers[index2])
            case (true, false):    return (sortedUsers[index1], nil)
            case (false, true):    return (nil, sortedUsers[index2])
            case (false, false):   return (nil, nil)
        }
    }
    
    // MARK: - Selectors
    /// Reads inputs from textfields, creates a new User instances, and adds to users array
    @objc func handleAddTapped() {
        guard let nameText = nameTextField.text, !nameText.isEmpty,
              let genderText = genderTextField.text?.lowercased(), !genderText.isEmpty else { return }
        let age = Int(ageTextField.text ?? "")
        
        let user = User(name: nameText, age: age, gender: Gender(rawValue: genderText) ?? .male)
        users.append(user)
    }
    
    /// Calling the 4 methods, and printing their results
    @objc func handlePrintTapped() {
        let sortAlphabetically = sortAlphabetically()
        let sortYoungestToOldest = sortYoungestToOldest()
        let ageGroups = userDictionaryOfAgeGroups()
        let pairs = pairFromAlphabeticalList(1, 2) // Indexes can be anything
        
        print("Original List: ", users, "\n")
        print("Alphabetically: ", sortAlphabetically, "\n")
        print("Youngest To Oldest: ", sortYoungestToOldest, "\n")
        print("Age Groups: ", ageGroups, "\n")
        print("Pairs: ", pairs, "\n")
    }
}

